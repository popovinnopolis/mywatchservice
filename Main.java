package myWatchService;

import java.io.IOException;
import java.nio.file.*;
import java.util.List;

/**
 * Created by evgenijpopov on 16.06.17.
 */
public class Main {
    public static void main(String[] args) throws IOException, InterruptedException {
        String filename = "test2.txt";
        Path filename2 = Paths.get("/Users/evgenijpopov/Documents/Innopolis/test2.txt");
        Path path = Paths.get("/Users/evgenijpopov/Documents/Innopolis/");

        WatchService watchService = FileSystems.getDefault().newWatchService();

        path.register(watchService, StandardWatchEventKinds.ENTRY_MODIFY);

        WatchKey key;

        List<String> contentBefore = Files.readAllLines(filename2);


        while ((key = watchService.take()) != null) {
            for (WatchEvent<?> event : key.pollEvents()) {
                if (event.context().toString().equals(filename)) {
                    System.out.printf("Event kind: %s. File affected: %s.\n", event.kind(), event.context());
                }
            }
            key.reset();

            List<String> contentAfter = Files.readAllLines(filename2);
            System.out.println(contentBefore);
            System.out.println(contentAfter);
            contentBefore = contentAfter;
        }
    }
}
